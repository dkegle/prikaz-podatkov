#### Prikaz podatkov

Uporabniku prikažemo podatke in osnovne statistične informacije.

Za inštalacijo in prikaz v brskalniku poženemo:
```
npm install
npm run watch
```
Demo je tedaj viden na localhost http://localhost:8080/.